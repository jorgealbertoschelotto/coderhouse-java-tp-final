package com.schelottojorge.tpfinal.entities;

import com.schelottojorge.tpfinal.service.WorldTimeService;
import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Entity
public class Product {
    // Propiedades
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String barCode;


    private String name;


    private String description;

    private int stock;

    private double price;

    private boolean status;

    private LocalDateTime createdAt;

    private LocalDateTime lastUpdated;
    private transient WorldTimeService worldTimeService = new WorldTimeService();

    @PrePersist
    private void loadData(){
        this.status = true;
        this.createdAt = worldTimeService.getCurrentDate();
    }

    @PreUpdate
    private void updateData(){
        this.lastUpdated = LocalDateTime.now();
    }
}
