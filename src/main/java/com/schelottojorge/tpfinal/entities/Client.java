package com.schelottojorge.tpfinal.entities;

import com.schelottojorge.tpfinal.service.WorldTimeService;
import jakarta.persistence.*;
import lombok.Data;
import java.time.LocalDate;
import java.time.LocalDateTime;


@Data
@Entity
@Table(name = "clients")
public class Client {

    // Propiedades
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    private String name;

    private String lastName;

    private String docNumber;

    private LocalDate fechNac;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @Column(name = "updated_at")
    private LocalDateTime updatedAt;

    private boolean status;

    private transient WorldTimeService worldTimeService = new WorldTimeService();

    @PrePersist
    private void loadCreatedAt(){
        this.createdAt = worldTimeService.getCurrentDate();
        this.status = true;
    }

    @PreUpdate
    public void loadUpdatedAt(){
        this.updatedAt = LocalDateTime.now();
    }


}
