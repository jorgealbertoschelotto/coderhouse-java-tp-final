package com.schelottojorge.tpfinal.entities;

import com.schelottojorge.tpfinal.service.WorldTimeService;
import jakarta.persistence.*;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Entity
public class Invoice {

    //Constructor

    public Invoice() {
    }

    // Propiedades
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String status;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @ManyToOne
    private Client client;

    @OneToMany(mappedBy = "invoice")
    private List<InvoiceDetail> invoiceDetail;

    private double total;

    private double sellPrice;


    private transient WorldTimeService worldTimeService = new WorldTimeService();

    @PrePersist
    private void loadDate(){
        this.createdAt = worldTimeService.getCurrentDate();
    }


}
