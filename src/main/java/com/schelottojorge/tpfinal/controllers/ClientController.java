// Clase ClientController
package com.schelottojorge.tpfinal.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.schelottojorge.tpfinal.dto.ClientDTO;
import com.schelottojorge.tpfinal.entities.Client;
import com.schelottojorge.tpfinal.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/api/v1/client")
public class ClientController {

    @Autowired
    private ClientService clienteService;

    @Autowired
    private ObjectMapper mapper; // Asegúrate de tener esta dependencia configurada

    // Método para calcular la edad a partir de la fecha de nacimiento
    private Integer calculateAge(LocalDate dateOfBirth) {
        if (dateOfBirth != null) {
            LocalDate currentDate = LocalDate.now();
            return Period.between(dateOfBirth, currentDate).getYears();
        } else {
            return null;
        }
    }

    @GetMapping("/list")
    public List<ClientDTO> listClients(){
        List<Client> clients = clienteService.showAll();
        List<ClientDTO> clientDTOs = new ArrayList<>();
        for (Client client : clients) {
            ClientDTO clientDTO = mapper.convertValue(client, ClientDTO.class);
            clientDTO.setAge(calculateAge(client.getFechNac()));
            clientDTOs.add(clientDTO);
        }
        return clientDTOs;
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> readClientById(@PathVariable Long id) {
        ClientDTO clientDTO = clienteService.findClientById(id);
        if (clientDTO != null) {
            clientDTO.setAge(calculateAge(clientDTO.getFechNac()));
            return ResponseEntity.ok(clientDTO);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping(path = "/update/{id}")
    public ResponseEntity<ClientDTO> uppdated(@PathVariable Long id,
                                              @RequestBody ClientDTO newClient) {
        return new ResponseEntity<>(this.clienteService.update(id, newClient), HttpStatus.OK);
    }

    @PostMapping(path = "/create")
    public ResponseEntity<ClientDTO> create(@RequestBody ClientDTO newClient) {
        return new ResponseEntity<>(this.clienteService.create(newClient), HttpStatus.OK);
    }

    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<ClientDTO> delete(@PathVariable Long id) {
        return new ResponseEntity<>(this.clienteService.delete(id), HttpStatus.OK);
    }


}
