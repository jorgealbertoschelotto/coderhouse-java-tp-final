package com.schelottojorge.tpfinal.controllers;

import com.schelottojorge.tpfinal.dto.InvoiceDTO;
import com.schelottojorge.tpfinal.entities.Invoice;
import com.schelottojorge.tpfinal.entities.Product;
import com.schelottojorge.tpfinal.service.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/api/v1/invoice")
public class InvoiceController {
    /**
     * Clase que Establece las consultas REST a ejecutar en los endpoints para invoice (factura de compra).
     */

    @Autowired
    private InvoiceService invoiceService;


    @GetMapping ("/{id}")
    public ResponseEntity<InvoiceDTO> readInvoiceById(@PathVariable Long id) throws Exception{

        return new ResponseEntity<>(this.invoiceService.findInvoiceById(id), HttpStatus.OK);
    }


    @PostMapping("/create")
    public ResponseEntity<InvoiceDTO> createInvoice(@RequestBody InvoiceDTO invoice) throws Exception{
        return new ResponseEntity<>(this.invoiceService.create(invoice), HttpStatus.OK);
    }




    // TODO: ADAPTAR A USO DE DTO
    // @PutMapping("/modificarfactura/{id}")
    // public ResponseEntity<?> updateInvoice(@PathVariable Long id) throws Exception {
    //     return new ResponseEntity<>(this.invoiceService.findInvoiceById(id), HttpStatus.OK);
//
    // }


    //@DeleteMapping("/bajafactura/{id}")
    //public ResponseEntity<?> deleteInvoice(@PathVariable Long id) throws Exception {
    //    InvoiceDTO deleteInvoice = invoiceService.findInvoiceById(id);
    //    invoiceService.delete(deleteInvoice.get());
    //}
}
