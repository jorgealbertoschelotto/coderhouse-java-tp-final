package com.schelottojorge.tpfinal.controllers;


import com.schelottojorge.tpfinal.dto.ProductDTO;
import com.schelottojorge.tpfinal.entities.Product;
import com.schelottojorge.tpfinal.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping(path = "/api/v1/product")
public class ProductController {


    @Autowired
    private ProductService productService;

    @GetMapping("/list")
    public ResponseEntity<List<ProductDTO>> listProducts(){

        // return productsRepository.findAll();
        return new ResponseEntity<>(this.productService.showAll(), HttpStatus.OK);
    }


    @PostMapping("/create")
    public ResponseEntity<ProductDTO>  createProduct(@RequestBody ProductDTO newProduct) throws Exception {
        return new ResponseEntity<>(this.productService.create(newProduct), HttpStatus.OK);
    }

    @GetMapping ("/{id}")
    public ResponseEntity<ProductDTO> readProductById(@PathVariable Long id){
        // Product readProduct = productsRepository.findById(id).get();
        return new ResponseEntity<>(this.productService.findProductById(id), HttpStatus.OK);

    }

    @PutMapping("/update/{id}")
    public ResponseEntity<ProductDTO> updateProduct(@PathVariable Long id, @RequestBody ProductDTO product) throws Exception {
        return new ResponseEntity<>(this.productService.update(id,product), HttpStatus.OK);

    }


    @DeleteMapping("/delete/{id}")
    public ResponseEntity<ProductDTO> deleteProduct(@PathVariable Long id) throws Exception {
        return new ResponseEntity<>(this.productService.delete(id), HttpStatus.OK);


    }

    @PutMapping(path = "/{id}/stock")
    public ResponseEntity<ProductDTO> discountStock(@PathVariable Long id,
                                                    @RequestBody Map<String, Integer> body) throws Exception {
        return new ResponseEntity<>(this.productService.discountStock(id,body.get("stock")),HttpStatus.OK);

    }
}
