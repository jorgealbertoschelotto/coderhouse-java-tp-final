package com.schelottojorge.tpfinal.controllers;

import com.schelottojorge.tpfinal.entities.InvoiceDetail;
import com.schelottojorge.tpfinal.service.InvoiceDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(path = "/api/v1/invoice/detail")
public class InvoiceDetailController {

    @Autowired
    private InvoiceDetailService invoiceDetailService;

    @PostMapping("/create")
    public String createInvoiceDetail(@RequestBody InvoiceDetail invoice){
        invoiceDetailService.create(invoice);
        return ("Se creo el detalle: " + invoice.getId());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> readInvoiceDetailById(@PathVariable Long id){

        Optional<InvoiceDetail> readInvoiceDetail = invoiceDetailService.findInvoiceDetailById(id);

        if (readInvoiceDetail.isPresent()) {
            return ResponseEntity.ok(readInvoiceDetail);
        } else {
            return ResponseEntity.notFound().build();
        }

    }


    @PutMapping("/update/{id}")
    public ResponseEntity<?> updateInvoiceDetail(@PathVariable Long id, @RequestBody InvoiceDetail cliente){
        Optional<InvoiceDetail> updateInvoiceDetail = invoiceDetailService.findInvoiceDetailById(id);

        if(updateInvoiceDetail.isPresent()){
            invoiceDetailService.update(updateInvoiceDetail, cliente);
            return ResponseEntity.ok(updateInvoiceDetail);
        }else{
            return ResponseEntity.notFound().build();
        }

    }


    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteInvoiceDetail(@PathVariable Long id){

        Optional<InvoiceDetail> deleteInvoiceDetail = invoiceDetailService.findInvoiceDetailById(id);

        if(deleteInvoiceDetail.isPresent()){
            invoiceDetailService.delete(deleteInvoiceDetail.get());
            return ResponseEntity.ok(deleteInvoiceDetail);
        }else{
            return ResponseEntity.notFound().build();
        }


    }

}
