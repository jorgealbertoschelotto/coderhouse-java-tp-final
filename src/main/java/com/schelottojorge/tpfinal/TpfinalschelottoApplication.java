package com.schelottojorge.tpfinal;

import com.schelottojorge.tpfinal.entities.Client;
import com.schelottojorge.tpfinal.entities.Invoice;
import com.schelottojorge.tpfinal.entities.InvoiceDetail;
import com.schelottojorge.tpfinal.entities.Product;
import com.schelottojorge.tpfinal.repository.ClientRepository;
import com.schelottojorge.tpfinal.repository.InvoiceDetailsRepository;
import com.schelottojorge.tpfinal.repository.InvoiceRepository;
import com.schelottojorge.tpfinal.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SpringBootApplication
public class TpfinalschelottoApplication implements CommandLineRunner {

	@Autowired
	ClientRepository clientRepository;

	@Autowired
	ProductRepository productRepository;

	@Autowired
	InvoiceRepository invoiceRepository;

	@Autowired
	InvoiceDetailsRepository invoiceDetailsRepository;

	public static void main(String[] args) {
		SpringApplication.run(TpfinalschelottoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		// CLIENTE
		Client cliente = new Client();
		cliente.setFechNac(LocalDate.parse("1985-12-06"));
		cliente.setName("Ramon");
		cliente.setLastName("Manzilla");
		this.clientRepository.save(cliente);


		Client cliente2 = new Client();
		cliente2.setFechNac(LocalDate.parse("1984-12-06"));
		cliente2.setName("Daniel");
		cliente2.setLastName("Morel");
		this.clientRepository.save(cliente2);


		Client cliente3 = new Client();
		cliente3.setFechNac(LocalDate.parse("1986-12-06"));
		cliente3.setName("Ana");
		cliente3.setLastName("Morel");
		this.clientRepository.save(cliente3);

		// PRODUCTO
		Product product = new Product();
		product.setDescription("Mayonesa Hellmans");
		product.setStock(10);
		product.setBarCode("123456789");
		product.setPrice(1500);
		this.productRepository.save(product);


		Product product2 = new Product();
		product2.setDescription("Paty de soja");
		product2.setStock(20);
		product2.setBarCode("123456790");
		product2.setPrice(2500);
		this.productRepository.save(product2);


		Product product3 = new Product();
		product3.setDescription("Pan");
		product3.setStock(30);
		product3.setBarCode("123456791");
		product3.setPrice(2000);
		this.productRepository.save(product3);


		// INVOICE & DETAIL
		Invoice invoice = new Invoice();
		invoice.setClient(cliente);

		// Guardar la factura
		invoiceRepository.save(invoice);


		// detail
		InvoiceDetail invoiceDetail = new InvoiceDetail();
		invoiceDetail.setProduct(product);
		invoiceDetail.setAmount(2);
		invoiceDetail.setUnitPrice(product.getPrice());

		InvoiceDetail invoiceDetail2 = new InvoiceDetail();
		invoiceDetail2.setProduct(product2);
		invoiceDetail2.setAmount(2);
		invoiceDetail2.setUnitPrice(product2.getPrice());

		// Asignar la factura al detalle de la factura
		invoiceDetail.setInvoice(invoice);
		invoiceDetail2.setInvoice(invoice);

		// Guardar el detalle de la factura
		invoiceDetailsRepository.save(invoiceDetail);

	}
}
