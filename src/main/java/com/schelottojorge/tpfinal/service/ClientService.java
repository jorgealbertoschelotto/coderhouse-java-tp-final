package com.schelottojorge.tpfinal.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.schelottojorge.tpfinal.dto.ClientDTO;
import com.schelottojorge.tpfinal.repository.ClientRepository;
import com.schelottojorge.tpfinal.entities.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Optional;

@Service
public class ClientService {
    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ObjectMapper mapper;

    public ClientDTO create(ClientDTO cliente){
        Optional<Client> clientOp = this.clientRepository.findById(cliente.getId());


        Client clientModel = mapper.convertValue(cliente, Client.class);
        clientModel = this.clientRepository.save(clientModel);
        cliente.setId(clientModel.getId());

        return cliente;
    }

    public List<Client> showAll(){
        return clientRepository.findAll();
    }

    public ClientDTO findClientById(Long idClient) {
        Optional<Client> readClient = this.clientRepository.findById(idClient);
        if (readClient.isPresent()) {
            return mapper.convertValue(readClient.get(), ClientDTO.class);
        } else {
            // Manejar el caso en que el cliente no se encuentra
            return null; // O puedes lanzar una excepción o manejarlo de otra manera según tu lógica de negocio
        }
    }

    public ClientDTO update(Long idClient, ClientDTO clientNew){
        if (idClient <= 0) {
            throw new IllegalArgumentException("ID no valido");
        }
        Optional<Client> clientOld = this.clientRepository.findById(idClient);

        clientOld.get().setName(clientNew.getName());
        clientOld.get().setLastName(clientNew.getLastname());
        clientOld.get().setDocNumber(clientNew.getDocNumber());
        clientOld.get().setFechNac(clientNew.getFechNac());

        Client clientToSave = this.clientRepository.save(clientOld.get());

        return mapper.convertValue(clientToSave, ClientDTO.class);
    }

    public ClientDTO delete(Long idClient){
        if (idClient <= 0) {
            throw new IllegalArgumentException("ID no valido");
        }
        Optional<Client> deleteClient = this.clientRepository.findById(idClient);


        // Borrado logico
        deleteClient.get().setStatus(false);
        this.clientRepository.save(deleteClient.get());
        return mapper.convertValue(deleteClient, ClientDTO.class);
    }

}
