package com.schelottojorge.tpfinal.service;


import com.schelottojorge.tpfinal.entities.InvoiceDetail;
import com.schelottojorge.tpfinal.repository.InvoiceDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class InvoiceDetailService {

    @Autowired
    private InvoiceDetailsRepository InvoiceDetailsRepository;

    public List<InvoiceDetail> showAll(){
        return InvoiceDetailsRepository.findAll();
    }

    public InvoiceDetail create(InvoiceDetail invoiceDetail){
        return InvoiceDetailsRepository.save(invoiceDetail);
    }

    public Optional<InvoiceDetail> findInvoiceDetailById(Long idInvoiceDetail){
        return InvoiceDetailsRepository.findById(idInvoiceDetail);
    }

    public InvoiceDetail update(Optional<InvoiceDetail> invoiceDetail, InvoiceDetail newInvoiceDetail){
        invoiceDetail.get().setInvoice(newInvoiceDetail.getInvoice());
        invoiceDetail.get().setAmount(newInvoiceDetail.getAmount());
        invoiceDetail.get().setUnitPrice(newInvoiceDetail.getUnitPrice());
        invoiceDetail.get().setProduct(newInvoiceDetail.getProduct());
        return InvoiceDetailsRepository.save(invoiceDetail.get());
    }

    public void delete(InvoiceDetail invoiceDetail){
        InvoiceDetailsRepository.delete(invoiceDetail);
    }
}
