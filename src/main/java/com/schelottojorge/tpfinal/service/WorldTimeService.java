package com.schelottojorge.tpfinal.service;


import com.schelottojorge.tpfinal.restapi.WorldClockResponse;
import org.springframework.boot.web.client.RestTemplateBuilder;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
public class WorldTimeService {

    private RestTemplate restTemplate = new RestTemplate();



    public LocalDateTime getCurrentDate() {
        String apiUrl = "https://worldtimeapi.org/api/timezone/america/argentina/buenos_aires";

        // Realiza la solicitud HTTP y obtener la respuesta como un objeto Java
        WorldClockResponse response = restTemplate.getForObject(apiUrl, WorldClockResponse.class);

        // Extrae la fecha y hora de la respuesta y convertirla a LocalDateTime
        String currentDateTime = response != null ? response.getCurrentDateTime() : null;

        if (currentDateTime != null) {
            // Ajusta el formato para asegurarse de que sea compatible
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSSXXX");

            // Parsea la cadena en un LocalDateTime
            return LocalDateTime.parse(currentDateTime, formatter);
        } else {
            // Si la respuesta es nula, devuelve la fecha y hora actual en el formato ISO 8601
            return LocalDateTime.now();
        }
    }
}
