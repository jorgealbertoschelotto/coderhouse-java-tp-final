package com.schelottojorge.tpfinal.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.schelottojorge.tpfinal.dto.ProductDTO;
import com.schelottojorge.tpfinal.entities.Product;
import com.schelottojorge.tpfinal.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.stream.Collectors;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ObjectMapper mapper;

    public List<ProductDTO> showAll(){

        return productRepository.findAll().stream()
                .map(product -> ProductDTO.builder()
                        .id(product.getId())
                        .barCode(product.getBarCode())
                        .description(product.getDescription())
                        .price(product.getPrice())
                        .stock(product.getStock())
                        .build())
                .collect(Collectors.toList());
    }

    public ProductDTO create(ProductDTO newProduct) throws Exception {
        Optional<Product> productBD = this.productRepository.findById(newProduct.getId());
        if (productBD.isPresent()){
            throw new Exception("Ya existe un producto con el codigo " + newProduct.getId() +" . Verificar");
        }

        Product productModel = mapper.convertValue(newProduct,Product.class);
        productModel = this.productRepository.save(productModel);
        return mapper.convertValue(productModel, ProductDTO.class);
    }


    public ProductDTO findProductById(Long idProduct){
        Optional<Product> product = productRepository.findById(idProduct);

        return mapper.convertValue(product, ProductDTO.class);
    }


    public ProductDTO update(Long id, ProductDTO newProduct) throws Exception {
        Optional<Product> product = productRepository.findById(id);
        if (product.isPresent()) {
            throw  new Exception("No existe ningun producto con el id brindado. Verificar");
        }

        Product productBD = mapper.convertValue(newProduct, Product.class);
        productBD.setId(id);
        return mapper.convertValue(this.productRepository.save(productBD), ProductDTO.class);
    }

    public ProductDTO delete(Long idProduct) throws Exception {
        if (idProduct <= 0){
            throw new Exception("El id brindado no es valido. Verificar");
        }

        Product product = productRepository.findById(idProduct)
                .orElseThrow(() -> new Exception("No existe ningun producto con el id brindado. Verificar"));


        product.setStatus(false);
        this.productRepository.save(product); // Baja logica.
        return mapper.convertValue(product, ProductDTO.class);

    }

    public ProductDTO discountStock(Long id, int stockToDiscount) throws Exception {
        if (id <= 0){
            throw new Exception("El id brindado no es valido. Verificar");
        }

        Product productModel = this.productRepository.findById(id)
                .orElseThrow(() -> new Exception("No existe ningun producto con el id brindado. Verificar"));


        productModel.setStock(productModel.getStock() - stockToDiscount);
        return mapper.convertValue(this.productRepository.save(productModel), ProductDTO.class);
    }

}
