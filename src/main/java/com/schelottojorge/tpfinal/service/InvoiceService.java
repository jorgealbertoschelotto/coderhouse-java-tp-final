package com.schelottojorge.tpfinal.service;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.schelottojorge.tpfinal.dto.InvoiceDTO;
import com.schelottojorge.tpfinal.dto.InvoiceDetailDTO;
import com.schelottojorge.tpfinal.entities.Client;
import com.schelottojorge.tpfinal.entities.Invoice;
import com.schelottojorge.tpfinal.entities.InvoiceDetail;
import com.schelottojorge.tpfinal.entities.Product;
import com.schelottojorge.tpfinal.repository.InvoiceDetailsRepository;
import com.schelottojorge.tpfinal.repository.InvoiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;


@Service
public class InvoiceService {

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private InvoiceDetailsRepository invoiceDetailRepository;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private ProductService productService;

    @Autowired
    private ClientService clientService;

    @Autowired
    private InvoiceDetailService invoiceDetailService;



    public InvoiceDTO findInvoiceById(Long idInvoice) throws Exception {
        if (idInvoice <= 0){
            throw new IllegalArgumentException("El id de la factura es incorrecto. Verificar");
        }

        return mapper.convertValue(this.invoiceRepository.findById(idInvoice)
                .orElseThrow(() -> new Exception("La factura solicitada no existe. Verificar")),InvoiceDTO.class);

    }

    public InvoiceDTO create(InvoiceDTO invoice) throws Exception {


        Invoice firstInvoice = new Invoice();
        Client client = mapper.convertValue(this.clientService.findClientById(invoice.getClient().getId()), Client.class);
        firstInvoice.setClient(client);


        List<InvoiceDetail> invoiceDetail = new ArrayList<>();
        double total = 0.0;
        for (InvoiceDetailDTO item: invoice.getInvoiceDetail()) {
            InvoiceDetail i = new InvoiceDetail();

            i.setInvoice(firstInvoice);

            i.setProduct(mapper.convertValue(this.productService.findProductById(item.getProduct().getId()), Product.class));
            this.productService.discountStock(item.getProduct().getId(), item.getAmount());
            i.setAmount(item.getAmount());
            i.setUnitPrice(i.getProduct().getPrice());
            i.setSubtotal(item.getAmount() * i.getProduct().getPrice());

            total += i.getSubtotal();
            invoiceDetail.add(i);
        }


        firstInvoice.setTotal(total);
        firstInvoice = this.invoiceRepository.save(firstInvoice);


        firstInvoice.setInvoiceDetail(this.invoiceDetailRepository.saveAll(invoiceDetail));

        return this.findInvoiceById(firstInvoice.getId());
    }


    /// TODO: CAMBIAR A PATRON DTO
    //public List<Invoice> showAll(){return invoiceRepository.findAll();}
    //public Invoice update(Optional<Invoice> invoice, Invoice newInvoice){
    //    invoice.get().setClient(newInvoice.getClient());
    //    invoice.get().setTotal(newInvoice.getTotal());
    //    invoice.get().setCreatedAt(newInvoice.getCreatedAt());
    //    return invoiceRepository.save(invoice.get());
    //}

    //public void delete(Invoice invoice){
    //    invoiceRepository.delete(invoice);
    //}



}
