package com.schelottojorge.tpfinal.repository;

import com.schelottojorge.tpfinal.entities.InvoiceDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InvoiceDetailsRepository extends JpaRepository<InvoiceDetail, Long> {
}
