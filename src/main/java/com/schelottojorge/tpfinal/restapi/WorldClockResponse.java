package com.schelottojorge.tpfinal.restapi;


import com.fasterxml.jackson.annotation.JsonProperty;

public class WorldClockResponse {
    @JsonProperty("datetime")
    private String currentDateTime;

    public String getCurrentDateTime() {
        return currentDateTime;
    }

    public void setCurrentDateTime(String currentDateTime) {
        this.currentDateTime = currentDateTime;
    }
}
