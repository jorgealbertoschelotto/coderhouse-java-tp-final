package com.schelottojorge.tpfinal.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import com.schelottojorge.tpfinal.entities.InvoiceDetail;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class InvoiceDetailDTO {

    private Long id;

    private InvoiceDTO invoice;

    private ProductDTO product;

    private int amount;

    private double unitPrice;


    private double subtotal;


}
