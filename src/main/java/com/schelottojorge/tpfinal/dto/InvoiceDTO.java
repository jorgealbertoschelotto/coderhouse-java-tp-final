package com.schelottojorge.tpfinal.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.schelottojorge.tpfinal.entities.InvoiceDetail;
import lombok.Data;
import java.time.LocalDateTime;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class InvoiceDTO {

    private String id;

    private String status;

    private ClientDTO client;

    private List<InvoiceDetailDTO> invoiceDetail;

    private double total;

    private LocalDateTime createdAt;
}
