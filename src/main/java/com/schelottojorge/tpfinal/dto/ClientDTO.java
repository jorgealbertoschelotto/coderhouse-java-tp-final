package com.schelottojorge.tpfinal.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ClientDTO {

    private Long id;

    private String name;

    private String lastname;

    private String docNumber;

    private LocalDate fechNac;

    private Integer age;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;



}
