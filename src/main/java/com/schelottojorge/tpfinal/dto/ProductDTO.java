package com.schelottojorge.tpfinal.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductDTO {

    private Long id;

    private String barCode;

    private String name;

    private String description;

    private int stock;

    private double price;

    private double sellPrice;

    private boolean status;

    private LocalDateTime createdAt;

    private LocalDateTime lastUpdated;
}
